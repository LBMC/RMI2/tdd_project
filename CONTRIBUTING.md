Labaronne Emmanuel
![RandomForest](https://img.shields.io/badge/R-Random_Forest_setup-blue?style=flat&labelColor=white&logo=r&logoColor=blue)
![Figures](https://img.shields.io/badge/R-Figures_Creation-blue?style=flat&labelColor=white&logo=r&logoColor=blue)

Cluet David
![Rstudio](https://img.shields.io/badge/Rstudio-Rmarkdown_adaptation-blue?style=flat&labelColor=white&logo=RStudio&logoColor=blue)
![Shiny](https://img.shields.io/badge/Rstudio-Shiny_Interface-blue?style=flat&labelColor=white&logo=RStudio&logoColor=blue)
![Readme](https://img.shields.io/badge/Git-README-red?style=flat&labelColor=white&logo=git&logoColor=red)
![git](https://img.shields.io/badge/Git-Repository_Maintenance-red?style=flat&labelColor=white&logo=git&logoColor=red)

Ricci Emiliano
![management](https://img.shields.io/badge/Project-Management-yellow)
![Readme](https://img.shields.io/badge/Git-README_corrections-red?style=flat&labelColor=white&logo=git&logoColor=red)
