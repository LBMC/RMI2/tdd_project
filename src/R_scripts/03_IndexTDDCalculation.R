######    Calcul de l'index TDD   
######         2020.01.14     

# A partir des valeurs normalisées ("db)

# 0. Set env --------------------------------------------------------------

setwd("~/RMI2/Projet_TDD/20200114_Normalisation")

pks <- list('ggplot2' , 'tidyverse' , 'DESeq2' , 'reshape2', "FactoMineR", "factoextra", 'RColorBrewer')
lapply(pks , library , character.only = T, quietly = FALSE)
theme_set(theme_bw())

load(file = "results/01_dbNormCountsAll_exon.RData")
load(file = "results/02_stability_of_genes.RData")


# 1. LymphoR -----------------------------------------------

t1Unt = dball[dball$cell == "Lympho" &
                dball$`Activated/Resting` == "Resting" &
                dball$time == "1h" & dball$treatment == "untreated",]
t3Unt = dball[dball$cell == "Lympho" &
                dball$`Activated/Resting` == "Resting" &
                dball$time == "3h" & dball$treatment == "untreated",]
t0Trip = dball[dball$cell == "Lympho" &
                 dball$`Activated/Resting` == "Resting" &
                 dball$time == "0h" & dball$treatment == "Trip",]
t1Trip = dball[dball$cell == "Lympho" &
                 dball$`Activated/Resting` == "Resting" &
                 dball$time == "1h" & dball$treatment == "Trip",]
t3Trip = dball[dball$cell == "Lympho" &
                 dball$`Activated/Resting` == "Resting" &
                 dball$time == "3h" & dball$treatment == "Trip",]
t1TripCHX = dball[dball$cell == "Lympho" &
                    dball$`Activated/Resting` == "Resting" &
                    dball$time == "1h" &
                    dball$treatment == "TripCHX",]
t3TripCHX = dball[dball$cell == "Lympho" &
                    dball$`Activated/Resting` == "Resting" &
                    dball$time == "3h" &
                    dball$treatment == "TripCHX",]
t1TripHAR = dball[dball$cell == "Lympho" &
                    dball$`Activated/Resting` == "Resting" &
                    dball$time == "1h" &
                    dball$treatment == "TripHarr",]
t3TripHAR = dball[dball$cell == "Lympho" &
                    dball$`Activated/Resting` == "Resting" &
                    dball$time == "3h" &
                    dball$treatment == "TripHarr",]

t1Unt = t1Unt[order(t1Unt$ensemblID, t1Unt$replicate),]
t3Unt = t3Unt[order(t3Unt$ensemblID, t3Unt$replicate),]
t0Trip = t0Trip[order(t0Trip$ensemblID, t0Trip$replicate),]
t1Trip = t1Trip[order(t1Trip$ensemblID, t1Trip$replicate),]
t3Trip = t3Trip[order(t3Trip$ensemblID, t3Trip$replicate),]
t1TripCHX = t1TripCHX[order(t1TripCHX$ensemblID, t1TripCHX$replicate),]
t3TripCHX = t3TripCHX[order(t3TripCHX$ensemblID, t3TripCHX$replicate),]
t1TripHAR = t1TripHAR[order(t1TripHAR$ensemblID, t1TripHAR$replicate),]
t3TripHAR = t3TripHAR[order(t3TripHAR$ensemblID, t3TripHAR$replicate),]

data_filtre = cbind(t1Unt[, c(1, 8)],
                    t1Unt[, 3],
                    t3Unt[, 3],
                    t0Trip[, 3],
                    t1Trip[, 3],
                    t3Trip[, 3],
                    t1TripCHX[, 3],
                    t3TripCHX[, 3],
                    t1TripHAR[, 3],
                    t3TripHAR[, 3])
colnames(data_filtre) = c(
  "ensemblID",
  "replicat",
  "t1Unt",
  "t3Unt",
  "t0Trip",
  "t1Trip",
  "t3Trip",
  "t1TripCHX",
  "t3TripCHX",
  "t1TripHAR",
  "t3TripHAR"
)

asTDDindex_LR_Triptolide = cbind(
  data_filtre,
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t1TripCHX"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t1TripHAR"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t1TripCHX"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t1TripHAR"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"])
)
colnames(asTDDindex_LR_Triptolide) = c(
  colnames(data_filtre),
  "asIndexTDD_t3TripTripCHX",
  "asIndexTDD_t1TripTripCHX",
  "asIndexTDD_t3TripTripHAR",
  "asIndexTDD_t1TripTripHAR",
  "relIndexTDD_t3TripTripCHX",
  "relIndexTDD_t1TripTripCHX",
  "relIndexTDD_t3TripTripHAR",
  "relIndexTDD_t1TripTripHAR"
)
asTDDindex_LR_Triptolide <- asTDDindex_LR_Triptolide[asTDDindex_LR_Triptolide$t3Trip > 10 , ]
asTDDindex_LR_Triptolide <- asTDDindex_LR_Triptolide[asTDDindex_LR_Triptolide$t0Trip > 10 , ]

## IndexTDD with untreated as reference

asTDDindex_LR_untreated = cbind(
  data_filtre,
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"]),
  (data_filtre[, "t1TripCHX"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t1Unt"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"]),
  (data_filtre[, "t1TripHAR"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t1Unt"]),
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t1TripCHX"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t1Unt"] - data_filtre[, "t1Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t1TripHAR"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t1Unt"] - data_filtre[, "t1Trip"])
)
colnames(asTDDindex_LR_untreated) = c(
  colnames(data_filtre),
  "asIndexTDD_t3TripTripCHX",
  "asIndexTDD_t1TripTripCHX",
  "asIndexTDD_t3TripTripHAR",
  "asIndexTDD_t1TripTripHAR",
  "relIndexTDD_t3TripTripCHX",
  "relIndexTDD_t1TripTripCHX",
  "relIndexTDD_t3TripTripHAR",
  "relIndexTDD_t1TripTripHAR"
)
asTDDindex_LR_untreated <- asTDDindex_LR_untreated[asTDDindex_LR_untreated$t3Trip > 10 , ]
asTDDindex_LR_untreated <- asTDDindex_LR_untreated[asTDDindex_LR_untreated$t3Unt > 10 , ]

## Correlation between this 2 asTDDindex

TDDindex_data <- merge(x = asTDDindex_LR_untreated,
                       y = asTDDindex_LR_Triptolide[,c("ensemblID",
                                                       "replicat",
                                                       "asIndexTDD_t3TripTripCHX",
                                                       "asIndexTDD_t1TripTripCHX",
                                                       "asIndexTDD_t3TripTripHAR",
                                                       "asIndexTDD_t1TripTripHAR",
                                                       "relIndexTDD_t3TripTripCHX",
                                                       "relIndexTDD_t1TripTripCHX",
                                                       "relIndexTDD_t3TripTripHAR",
                                                       "relIndexTDD_t1TripTripHAR")],
                       by = c("ensemblID", "replicat"),
                       suffixes = c(".untreated", ".0hTrip"))

TDDindexes_LR <-  as_tibble(TDDindex_data[!duplicated(TDDindex_data),])
save(TDDindexes_LR, file = "results/03_TDD_indexes_LR.RData")

# 2. Lympho A ---------------

## IndexTDD with Triptolide 0h as reference
t1Unt = dball[dball$cell == "Lympho" &
                dball$`Activated/Resting` == "Activated" &
                dball$time == "1h" & dball$treatment == "untreated",]
t3Unt = dball[dball$cell == "Lympho" &
                dball$`Activated/Resting` == "Activated" &
                dball$time == "3h" & dball$treatment == "untreated",]
t0Trip = dball[dball$cell == "Lympho" &
                 dball$`Activated/Resting` == "Activated" &
                 dball$time == "0h" & dball$treatment == "Trip",]
t1Trip = dball[dball$cell == "Lympho" &
                 dball$`Activated/Resting` == "Activated" &
                 dball$time == "1h" & dball$treatment == "Trip",]
t3Trip = dball[dball$cell == "Lympho" &
                 dball$`Activated/Resting` == "Activated" &
                 dball$time == "3h" & dball$treatment == "Trip",]
t1TripCHX = dball[dball$cell == "Lympho" &
                    dball$`Activated/Resting` == "Activated" &
                    dball$time == "1h" &
                    dball$treatment == "TripCHX",]
t3TripCHX = dball[dball$cell == "Lympho" &
                    dball$`Activated/Resting` == "Activated" &
                    dball$time == "3h" &
                    dball$treatment == "TripCHX",]
t1TripHAR = dball[dball$cell == "Lympho" &
                    dball$`Activated/Resting` == "Activated" &
                    dball$time == "1h" &
                    dball$treatment == "TripHarr",]
t3TripHAR = dball[dball$cell == "Lympho" &
                    dball$`Activated/Resting` == "Activated" &
                    dball$time == "3h" &
                    dball$treatment == "TripHarr",]


t0DRB = dball[dball$cell == "Lympho" &
                dball$`Activated/Resting` == "Activated" &
                dball$time == "0h" & dball$treatment == "DRB",]
t1DRB = dball[dball$cell == "Lympho" &
                dball$`Activated/Resting` == "Activated" &
                dball$time == "1h" & dball$treatment == "DRB",]
t3DRB = dball[dball$cell == "Lympho" &
                dball$`Activated/Resting` == "Activated" &
                dball$time == "3h" & dball$treatment == "DRB",]
t1DRBCHX = dball[dball$cell == "Lympho" &
                   dball$`Activated/Resting` == "Activated" &
                   dball$time == "1h" &
                   dball$treatment == "DRBCHX",]
t3DRBCHX = dball[dball$cell == "Lympho" &
                   dball$`Activated/Resting` == "Activated" &
                   dball$time == "3h" &
                   dball$treatment == "DRBCHX",]
t1DRBHAR = dball[dball$cell == "Lympho" &
                   dball$`Activated/Resting` == "Activated" &
                   dball$time == "1h" &
                   dball$treatment == "DRBHarr",]
t3DRBHAR = dball[dball$cell == "Lympho" &
                   dball$`Activated/Resting` == "Activated" &
                   dball$time == "3h" &
                   dball$treatment == "DRBHarr",]

t1Unt = t1Unt[order(t1Unt$ensemblID, t1Unt$replicate),]
t3Unt = t3Unt[order(t3Unt$ensemblID, t3Unt$replicate),]
t0Trip = t0Trip[order(t0Trip$ensemblID, t0Trip$replicate),]
t1Trip = t1Trip[order(t1Trip$ensemblID, t1Trip$replicate),]
t3Trip = t3Trip[order(t3Trip$ensemblID, t3Trip$replicate),]
t1TripCHX = t1TripCHX[order(t1TripCHX$ensemblID, t1TripCHX$replicate),]
t3TripCHX = t3TripCHX[order(t3TripCHX$ensemblID, t3TripCHX$replicate),]
t1TripHAR = t1TripHAR[order(t1TripHAR$ensemblID, t1TripHAR$replicate),]
t3TripHAR = t3TripHAR[order(t3TripHAR$ensemblID, t3TripHAR$replicate),]
t0DRB = t0DRB[order(t0DRB$ensemblID, t0DRB$replicate),]
t1DRB = t1DRB[order(t1DRB$ensemblID, t1DRB$replicate),]
t3DRB = t3DRB[order(t3DRB$ensemblID, t3DRB$replicate),]
t1DRBCHX = t1DRBCHX[order(t1DRBCHX$ensemblID, t1DRBCHX$replicate),]
t3DRBCHX = t3DRBCHX[order(t3DRBCHX$ensemblID, t3DRBCHX$replicate),]
t1DRBHAR = t1DRBHAR[order(t1DRBHAR$ensemblID, t1DRBHAR$replicate),]
t3DRBHAR = t3DRBHAR[order(t3DRBHAR$ensemblID, t3DRBHAR$replicate),]

data_filtre = cbind(t1Unt[, c(1, 8)],
                    t1Unt[, 3],
                    t3Unt[, 3],
                    t0Trip[, 3],
                    t1Trip[, 3],
                    t3Trip[, 3],
                    t1TripCHX[, 3],
                    t3TripCHX[, 3],
                    t1TripHAR[, 3],
                    t3TripHAR[, 3],
                    t0DRB[, 3],
                    t1DRB[, 3],
                    t3DRB[, 3],
                    t1DRBCHX[, 3],
                    t3DRBCHX[, 3],
                    t1DRBHAR[, 3],
                    t3DRBHAR[, 3])
colnames(data_filtre) = c(
  "ensemblID",
  "replicat",
  "t1Unt",
  "t3Unt",
  "t0Trip",
  "t1Trip",
  "t3Trip",
  "t1TripCHX",
  "t3TripCHX",
  "t1TripHAR",
  "t3TripHAR",
  "t0DRB",
  "t1DRB",
  "t3DRB",
  "t1DRBCHX",
  "t3DRBCHX",
  "t1DRBHAR",
  "t3DRBHAR"
)

asTDDindex_LA_Triptolide = cbind(
  data_filtre,
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t1TripCHX"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t1TripHAR"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t1TripCHX"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t1TripHAR"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t3DRBCHX"] - data_filtre[, "t3DRB"]) / ##################"
    (data_filtre[, "t0DRB"]),
  (data_filtre[, "t1DRBCHX"] - data_filtre[, "t1DRB"]) /
    (data_filtre[, "t0DRB"]),
  (data_filtre[, "t3DRBHAR"] - data_filtre[, "t3DRB"]) /
    (data_filtre[, "t0DRB"]),
  (data_filtre[, "t1DRBHAR"] - data_filtre[, "t1DRB"]) /
    (data_filtre[, "t0DRB"]),
  (data_filtre[, "t3DRBCHX"] - data_filtre[, "t3DRB"]) /
    (data_filtre[, "t0DRB"] - data_filtre[, "t3DRB"]),
  (data_filtre[, "t1DRBCHX"] - data_filtre[, "t1DRB"]) /
    (data_filtre[, "t0DRB"] - data_filtre[, "t3DRB"]),
  (data_filtre[, "t3DRBHAR"] - data_filtre[, "t3DRB"]) /
    (data_filtre[, "t0DRB"] - data_filtre[, "t3DRB"]),
  (data_filtre[, "t1DRBHAR"] - data_filtre[, "t1DRB"]) /
    (data_filtre[, "t0DRB"] - data_filtre[, "t3DRB"])
)
colnames(asTDDindex_LA_Triptolide) = c(
  colnames(data_filtre),
  "asIndexTDD_t3TripTripCHX",
  "asIndexTDD_t1TripTripCHX",
  "asIndexTDD_t3TripTripHAR",
  "asIndexTDD_t1TripTripHAR",
  "relIndexTDD_t3TripTripCHX",
  "relIndexTDD_t1TripTripCHX",
  "relIndexTDD_t3TripTripHAR",
  "relIndexTDD_t1TripTripHAR",
  "asIndexTDD_t3DRBDRBCHX",
  "asIndexTDD_t1DRBDRBCHX",
  "asIndexTDD_t3DRBDRBHAR",
  "asIndexTDD_t1DRBDRBHAR",
  "relIndexTDD_t3DRBDRBCHX",
  "relIndexTDD_t1DRBDRBCHX",
  "relIndexTDD_t3DRBDRBHAR",
  "relIndexTDD_t1DRBDRBHAR"
)
asTDDindex_LA_Triptolide <- asTDDindex_LA_Triptolide[asTDDindex_LA_Triptolide$t3Trip > 10 , ]
asTDDindex_LA_Triptolide <- asTDDindex_LA_Triptolide[asTDDindex_LA_Triptolide$t0Trip > 10 , ]

## IndexTDD with untreated as reference

asTDDindex_LA_untreated = cbind(
  data_filtre,
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"]),
  (data_filtre[, "t1TripCHX"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t1Unt"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"]),
  (data_filtre[, "t1TripHAR"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t1Unt"]),
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t1TripCHX"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t1Unt"] - data_filtre[, "t1Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t1TripHAR"] - data_filtre[, "t1Trip"]) /
    (data_filtre[, "t1Unt"] - data_filtre[, "t1Trip"]), 
  (data_filtre[, "t3DRBCHX"] - data_filtre[, "t3DRB"]) /
    (data_filtre[, "t3Unt"]),
  (data_filtre[, "t1DRBCHX"] - data_filtre[, "t1DRB"]) /
    (data_filtre[, "t1Unt"]),
  (data_filtre[, "t3DRBHAR"] - data_filtre[, "t3DRB"]) /
    (data_filtre[, "t3Unt"]),
  (data_filtre[, "t1DRBHAR"] - data_filtre[, "t1DRB"]) /
    (data_filtre[, "t1Unt"]),
  (data_filtre[, "t3DRBCHX"] - data_filtre[, "t3DRB"]) /
    (data_filtre[, "t3Unt"] - data_filtre[, "t3DRB"]),
  (data_filtre[, "t1DRBCHX"] - data_filtre[, "t1DRB"]) /
    (data_filtre[, "t1Unt"] - data_filtre[, "t1DRB"]),
  (data_filtre[, "t3DRBHAR"] - data_filtre[, "t3DRB"]) /
    (data_filtre[, "t3Unt"] - data_filtre[, "t3DRB"]),
  (data_filtre[, "t1DRBHAR"] - data_filtre[, "t1DRB"]) /
    (data_filtre[, "t1Unt"] - data_filtre[, "t1DRB"])
)
colnames(asTDDindex_LA_untreated) = c(
  colnames(data_filtre),
  "asIndexTDD_t3TripTripCHX",
  "asIndexTDD_t1TripTripCHX",
  "asIndexTDD_t3TripTripHAR",
  "asIndexTDD_t1TripTripHAR",
  "relIndexTDD_t3TripTripCHX",
  "relIndexTDD_t1TripTripCHX",
  "relIndexTDD_t3TripTripHAR",
  "relIndexTDD_t1TripTripHAR",
  "asIndexTDD_t3DRBDRBCHX",
  "asIndexTDD_t1DRBDRBCHX",
  "asIndexTDD_t3DRBDRBHAR",
  "asIndexTDD_t1DRBDRBHAR",
  "relIndexTDD_t3DRBDRBCHX",
  "relIndexTDD_t1DRBDRBCHX",
  "relIndexTDD_t3DRBDRBHAR",
  "relIndexTDD_t1DRBDRBHAR"
)
asTDDindex_LA_untreated <- asTDDindex_LA_untreated[asTDDindex_LA_untreated$t3Trip > 10 , ]
asTDDindex_LA_untreated <- asTDDindex_LA_untreated[asTDDindex_LA_untreated$t3Unt > 10 , ]

## Correlation between this 2 asTDDindex

TDDindex_data <- merge(x = asTDDindex_LA_untreated,
                       y = asTDDindex_LA_Triptolide[,c("ensemblID",
                                                       "replicat",
                                                       "asIndexTDD_t3TripTripCHX",
                                                       "asIndexTDD_t1TripTripCHX",
                                                       "asIndexTDD_t3TripTripHAR",
                                                       "asIndexTDD_t1TripTripHAR",
                                                       "relIndexTDD_t3TripTripCHX",
                                                       "relIndexTDD_t1TripTripCHX",
                                                       "relIndexTDD_t3TripTripHAR",
                                                       "relIndexTDD_t1TripTripHAR",
                                                       "asIndexTDD_t3DRBDRBCHX",
                                                       "asIndexTDD_t1DRBDRBCHX",
                                                       "asIndexTDD_t3DRBDRBHAR",
                                                       "asIndexTDD_t1DRBDRBHAR",
                                                       "relIndexTDD_t3DRBDRBCHX",
                                                       "relIndexTDD_t1DRBDRBCHX",
                                                       "relIndexTDD_t3DRBDRBHAR",
                                                       "relIndexTDD_t1DRBDRBHAR")],
                       by = c("ensemblID", "replicat"),
                       suffixes = c(".untreated", ".0hTrip"))

TDDindexes_LA <-  as_tibble(TDDindex_data[!duplicated(TDDindex_data),])
save(TDDindexes_LA, file = "results/03_TDD_indexes_LA.RData")

# 3. MacroR ---------------------------------------------------------------
t0Unt = dball[dball$cell == "macro" &
                dball$`Activated/Resting` == "Resting" &
                dball$time == "0h" & dball$treatment == "untreated",]
t3Unt = dball[dball$cell == "macro" &
                dball$`Activated/Resting` == "Resting" &
                dball$time == "3h" & dball$treatment == "untreated",]
t0Trip = dball[dball$cell == "macro" &
                 dball$`Activated/Resting` == "Resting" &
                 dball$time == "0h" & dball$treatment == "Trip",]
t3Trip = dball[dball$cell == "macro" &
                 dball$`Activated/Resting` == "Resting" &
                 dball$time == "3h" & dball$treatment == "Trip",]
t3TripCHX = dball[dball$cell == "macro" &
                    dball$`Activated/Resting` == "Resting" &
                    dball$time == "3h" &
                    dball$treatment == "TripCHX",]
t3TripHAR = dball[dball$cell == "macro" &
                    dball$`Activated/Resting` == "Resting" &
                    dball$time == "3h" &
                    dball$treatment == "TripHarr",]

t0Trip = t0Trip[!(t0Trip$librairies=="LPSno_macro_0h_Triptolide_i5_4_exon"),] # pas de réplicat i5 pour CHX
t3Unt = t3Unt[!(t3Unt$librairies=="LPSno_macro_3h_untreated_i5_3_exon"),] # pas de réplicat i5 pour CHX
t3Trip = t3Trip[!(t3Trip$librairies=="LPSno_macro_3h_Triptolide_i5_6_exon"),] # pas de réplicat i5 pour CHX

t0Unt = t0Unt[order(t0Unt$ensemblID, t0Unt$replicate),]
t3Unt = t3Unt[order(t3Unt$ensemblID, t3Unt$replicate),]
t0Trip = t0Trip[order(t0Trip$ensemblID, t0Trip$replicate),]
t3Trip = t3Trip[order(t3Trip$ensemblID, t3Trip$replicate),]
t3TripCHX = t3TripCHX[order(t3TripCHX$ensemblID, t3TripCHX$replicate),]
t3TripHAR = t3TripHAR[order(t3TripHAR$ensemblID, t3TripHAR$replicate),]

data_filtre = cbind(t3Unt[, c(1, 8)],
                    t3Unt[, 3],
                    t0Trip[, 3],
                    t3Trip[, 3],
                    t3TripCHX[, 3],
                    t3TripHAR[, 3])
colnames(data_filtre) = c(
  "ensemblID",
  "replicat",
  "t3Unt",
  "t0Trip",
  "t3Trip",
  "t3TripCHX",
  "t3TripHAR"
)

asTDDindex_MR_Triptolide = cbind(
  data_filtre,
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"])
  )

colnames(asTDDindex_MR_Triptolide) = c(
  colnames(data_filtre),
  "asIndexTDD_t3TripTripCHX",
  "asIndexTDD_t3TripTripHAR",
  "relIndexTDD_t3TripTripCHX",
  "relIndexTDD_t3TripTripHAR"
)
asTDDindex_MR_Triptolide <- asTDDindex_MR_Triptolide[asTDDindex_MR_Triptolide$t3Trip > 10 , ]
asTDDindex_MR_Triptolide <- asTDDindex_MR_Triptolide[asTDDindex_MR_Triptolide$t0Trip > 10 , ]

## IndexTDD with untreated as reference

asTDDindex_MR_untreated = cbind(
  data_filtre,
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"]),
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"] - data_filtre[, "t3Trip"])
)
colnames(asTDDindex_MR_untreated) = c(
  colnames(data_filtre),
  "asIndexTDD_t3TripTripCHX",
  "asIndexTDD_t3TripTripHAR",
  "relIndexTDD_t3TripTripCHX",
  "relIndexTDD_t3TripTripHAR"
)
asTDDindex_MR_untreated <- asTDDindex_MR_untreated[asTDDindex_MR_untreated$t3Trip > 10 , ]
asTDDindex_MR_untreated <- asTDDindex_MR_untreated[asTDDindex_MR_untreated$t3Unt > 10 , ]

## Correlation between this 2 asTDDindex

TDDindex_data <- merge(x = asTDDindex_MR_untreated,
                       y = asTDDindex_MR_Triptolide[,c("ensemblID",
                                                       "replicat",
                                                       "asIndexTDD_t3TripTripCHX",
                                                       "asIndexTDD_t3TripTripHAR",
                                                       "relIndexTDD_t3TripTripCHX",
                                                       "relIndexTDD_t3TripTripHAR")],
                       by = c("ensemblID", "replicat"),
                       suffixes = c(".untreated", ".0hTrip"))

TDDindexes_MR <-  as_tibble(TDDindex_data[!duplicated(TDDindex_data),])
save(TDDindexes_MR, file = "results/03_TDD_indexes_MR.RData")
# 4. MacroA ---------------------------------------------------------------

t3Unt = dball[dball$cell == "macro" &
                dball$`Activated/Resting` == "Activated" &
                dball$time == "3h" & dball$treatment == "untreated",]
t0Trip = dball[dball$cell == "macro" &
                 dball$`Activated/Resting` == "Activated" &
                 dball$time == "0h" & dball$treatment == "Trip",]
t3Trip = dball[dball$cell == "macro" &
                 dball$`Activated/Resting` == "Activated" &
                 dball$time == "3h" & dball$treatment == "Trip",]
t3TripCHX = dball[dball$cell == "macro" &
                    dball$`Activated/Resting` == "Activated" &
                    dball$time == "3h" &
                    dball$treatment == "TripCHX",]
t3TripHAR = dball[dball$cell == "macro" &
                    dball$`Activated/Resting` == "Activated" &
                    dball$time == "3h" &
                    dball$treatment == "TripHarr",]

t0Trip = t0Trip[!(t0Trip$librairies=="LPS_macro_0h_Triptolide_m4_4_exon"),] # pas de réplicat m4 pour CHX
t3Unt = t3Unt[!(t3Unt$librairies=="LPS_macro_3h_untreated_m4_3_exon"),] # pas de réplicat m4 pour CHX
t3Trip = t3Trip[!(t3Trip$librairies=="LPS_macro_3h_Triptolide_m4_6_exon"),] # pas de réplicat m4 pour CHX
t3TripHAR = t3TripHAR[!(t3TripHAR$librairies=="LPS_macro_3h_Triptolide_HARR_m4_14_exon"),] # pas de réplicat m4 pour CHX

t3Unt = t3Unt[order(t3Unt$ensemblID, t3Unt$replicate),]
t0Trip = t0Trip[order(t0Trip$ensemblID, t0Trip$replicate),]
t3Trip = t3Trip[order(t3Trip$ensemblID, t3Trip$replicate),]
t3TripCHX = t3TripCHX[order(t3TripCHX$ensemblID, t3TripCHX$replicate),]
t3TripHAR = t3TripHAR[order(t3TripHAR$ensemblID, t3TripHAR$replicate),]

data_filtre = cbind(t3Unt[, c(1, 8)],
                    t3Unt[, 3],
                    t0Trip[, 3],
                    t3Trip[, 3],
                    t3TripCHX[, 3],
                    t3TripHAR[, 3])
colnames(data_filtre) = c(
  "ensemblID",
  "replicat",
  "t3Unt",
  "t0Trip",
  "t3Trip",
  "t3TripCHX",
  "t3TripHAR"
)

asTDDindex_MA_Triptolide = cbind(
  data_filtre,
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"]),
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t0Trip"] - data_filtre[, "t3Trip"])
)

colnames(asTDDindex_MA_Triptolide) = c(
  colnames(data_filtre),
  "asIndexTDD_t3TripTripCHX",
  "asIndexTDD_t3TripTripHAR",
  "relIndexTDD_t3TripTripCHX",
  "relIndexTDD_t3TripTripHAR"
)
asTDDindex_MA_Triptolide <- asTDDindex_MA_Triptolide[asTDDindex_MA_Triptolide$t3Trip > 10 , ]
asTDDindex_MA_Triptolide <- asTDDindex_MA_Triptolide[asTDDindex_MA_Triptolide$t0Trip > 10 , ]

## IndexTDD with untreated as reference

asTDDindex_MA_untreated = cbind(
  data_filtre,
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"]),
  (data_filtre[, "t3TripCHX"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"] - data_filtre[, "t3Trip"]),
  (data_filtre[, "t3TripHAR"] - data_filtre[, "t3Trip"]) /
    (data_filtre[, "t3Unt"] - data_filtre[, "t3Trip"])
)
colnames(asTDDindex_MA_untreated) = c(
  colnames(data_filtre),
  "asIndexTDD_t3TripTripCHX",
  "asIndexTDD_t3TripTripHAR",
  "relIndexTDD_t3TripTripCHX",
  "relIndexTDD_t3TripTripHAR"
)
asTDDindex_MA_untreated <- asTDDindex_MA_untreated[asTDDindex_MA_untreated$t3Trip > 10 , ]
asTDDindex_MA_untreated <- asTDDindex_MA_untreated[asTDDindex_MA_untreated$t3Unt > 10 , ]

## Correlation between this 2 asTDDindex

TDDindex_data <- merge(x = asTDDindex_MA_untreated,
                       y = asTDDindex_MA_untreated[,c("ensemblID",
                                                       "replicat",
                                                       "asIndexTDD_t3TripTripCHX",
                                                       "asIndexTDD_t3TripTripHAR",
                                                       "relIndexTDD_t3TripTripCHX",
                                                       "relIndexTDD_t3TripTripHAR")],
                       by = c("ensemblID", "replicat"),
                       suffixes = c(".untreated", ".0hTrip"))

TDDindexes_MA <-  as_tibble(TDDindex_data[!duplicated(TDDindex_data),])
save(TDDindexes_MA, file = "results/03_TDD_indexes_MA.RData")

# 5. Save Results ---------------------------------------------------------

IndexTDD = list(TDDindexes_LR, TDDindexes_LA, TDDindexes_MR, TDDindexes_MA)
names(IndexTDD) = c("LymphoR", "LymphoA", "MacroR", "MacroA")
save(IndexTDD, file = "results/03_IndexTDDallConditions.RData")
